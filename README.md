Buutti Ldt assigment description:

Android / Java 
 
Create an android UI that has three different views, and use only one activity. These three views needs to share data, and the data needs to be persistent. 
 
The first view should contain two separate inputs to enter two different values.  - The value that is initially shown should be the same value that was put in the last time this view was shown. If it's the first time, the inputs should be empty. The second view should contain another two separate inputs to enter two different values. - The value that is initially shown should be the same value that was put in the last time this view was shown. If it's the first time, the inputs should be empty. 
 
The third view should present two values multiplied with eachother.  - The values it chooses to use (either from the first or the second view) should be dependent on which were put in last. 
 
If the application is paused, the application should always return in the same activity that it was paused from. If the application is closed, the application should start from the first view. 
 
The three views should be clearly distinct from eachother. You can freely choose layouts, but keep them simple. Choose a way to implement transitioning between the views. 
 
Evaluation criteria: 
● Design 
● Structure 
● Use of language features 
● Clarity of implementation 
● Error checking 
● Comments (is the code self documenting) 
● Testability 
● Documentation 
● Usability 