package buutti.fi.buuttitestapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import fragments.FirstLayoutFragment;
import fragments.SecondLayoutFragment;
import fragments.ResultLayoutFragment;
import utils.FieldNamesInterface;

import static utils.FieldNamesInterface.JSON_FILE;


public class MainActivity extends AppCompatActivity {

    /**
     * Main activity where app exution starts.
     *
     * Different views are shown as Fragments.
     */

    //Reciver that receives status updated what view should be visible att mainview
    MyViewStateReceiver myViewStateReceiver = new MyViewStateReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lets register broadcast receiver so it can receive view status updates from Fragments
        registerReceiver(myViewStateReceiver, new IntentFilter(FieldNamesInterface.BROADCAST_ACTION_TEXT));

        //Show the first view(Fragment) at mainview
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.replace(R.id.your_placeholder, new FirstLayoutFragment());

        ft.commit();

    }


    //Reciver picks up status updated. Related to switch view (Fragment) should be visible at mainview
    public class MyViewStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            Bundle extras = intent.getExtras();
            if (extras != null) {
                String broadCastAction = intent.getStringExtra(FieldNamesInterface.BROADCAST_ACTION_TEXT);

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                //Lets add some transtation animations
                ft.setCustomAnimations(R.anim.exit_right,
                        R.anim.exit_left);

                if (broadCastAction.contains(FieldNamesInterface.FIRST_VIEW_ACTION)) {
                   // Replace the contents of the container with the new fragment
                    ft.replace(R.id.your_placeholder, new FirstLayoutFragment());

                } else if (broadCastAction.contains(FieldNamesInterface.SECOND_VIEW_ACTION)) {

                    // Replace the contents of the container with the new fragment
                    ft.replace(R.id.your_placeholder, new SecondLayoutFragment());

                } else if (broadCastAction.contains(FieldNamesInterface.RESULT_VIEW_ACTION)) {

                    // Replace the contents of the container with the new fragment
                    ft.replace(R.id.your_placeholder, new ResultLayoutFragment());

                }

                ft.commit();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //Unregister the Broadcastreceiver so that is dosent leak.
        unregisterReceiver(myViewStateReceiver);


        SharedPreferences prefs = getSharedPreferences(
                getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        // use a default value using new Date()
        SharedPreferences.Editor editor = ((SharedPreferences) prefs).edit();

        //Lets remove saved Json file from Sharedpreferences
        editor.remove(JSON_FILE);

        editor.commit();
    }
}
