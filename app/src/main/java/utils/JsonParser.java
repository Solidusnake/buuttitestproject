package utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class JsonParser {

    /**
     * Class to parse HashMap to Json. And Json back to HashMap
     * @param numbers
     * @return
     */

    //Change the HashMap to Json
    public static String toJSon(HashMap<String, Long> numbers) {

        try {

            JSONObject jsonObj = new JSONObject();

            for(int i = 0; i<FieldNamesInterface.NAMES.length; i++){
                if(numbers.get(FieldNamesInterface.NAMES[i])!=null){
                    jsonObj.put(FieldNamesInterface.NAMES[i], numbers.get(FieldNamesInterface.NAMES[i])); // Set the first name/pair
                }
            }

            return jsonObj.toString();

        }
        catch(JSONException ex) {
            //ex.printStackTrace();
        }

        return null;

    }

    //Change the Json to HashMap
    public static HashMap<String, Long> fromJson(String json) {

        HashMap<String, Long> numbers = new HashMap<String, Long>();
        JSONObject jsonObj = null;
        try {

            jsonObj = new JSONObject(json);

        } catch(JSONException ex) {
            //ex.printStackTrace();
        }

        if(jsonObj==null){
            return numbers;
        }

        JSONArray names = jsonObj.names();

        for(int i = 0; i<names.length(); i++){
            try {
                if(jsonObj.get((String)names.get(i))!=null){
                    Object object = jsonObj.get((String)names.get(i));

                    numbers.put((String)names.get(i), Long.parseLong(object.toString())); // Set the first name/pair
                }
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        return numbers;
    }
}
