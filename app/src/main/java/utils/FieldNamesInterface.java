package utils;

/**
 * Class where is different contant variables
 */
public interface FieldNamesInterface {

    //Field names at saved to HashMap And Json. By adding more field names to this list. More data can be added both Json and HashMap
    public static String[] NAMES =  {"firstNumber", "secondNumber","thirdNumber", "fourdNumber", "result1"};

    //Broacastreceiver chosen action. The firstview comes visible
    public static String FIRST_VIEW_ACTION = "first_view";

    //Broacastreceiver chosen action. The secondview comes visible
    public static String SECOND_VIEW_ACTION = "second_view";

    //Broacastreceiver chosen action. The resultview comes visible
    public static String RESULT_VIEW_ACTION = "result_view";

    //Broacastreceiver action filter to chosing which view comes visible
    public static String BROADCAST_ACTION_TEXT = "broadCastAction";

    //File name where Json data is saved at SharedPreferences
    public static String JSON_FILE = "numbersFile";
}
