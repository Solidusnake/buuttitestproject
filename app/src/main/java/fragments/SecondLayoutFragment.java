package fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import androidx.fragment.app.Fragment;
import buutti.fi.buuttitestapplication.R;
import utils.FieldNamesInterface;
import utils.JsonParser;

import static utils.FieldNamesInterface.JSON_FILE;

public class SecondLayoutFragment extends Fragment {

    HashMap<String, Long> numbers = new HashMap<String, Long>();

    //Data is saved permanently to SharedPreferences as Json file
    SharedPreferences prefs = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        //Let get the Json file from Shared preferences. It contains numbers at Json form.
        prefs = getActivity().getSharedPreferences(
                getActivity().getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        //Let get Json file from SharedPreferences
        String json = ((SharedPreferences) prefs).getString(JSON_FILE, "");

        //Change the Json to HashMap form
        numbers = JsonParser.fromJson(json);

        return inflater.inflate(R.layout.second_two_inputs_view, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //The title of layout
        final TextView title = (TextView) view.findViewById(R.id.title);

        title.setText("Anna seuraavat luvut:");

        //The input field where user defines the input numbers
        final TextView oldNumber = (TextView) view.findViewById(R.id.old_number_text_1);

        final EditText newNumber = (EditText) view.findViewById(R.id.give_new_number_text_1);

        Button saveButton = (Button) view.findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number = oldNumber.getText().toString();

                String number2 = newNumber.getText().toString();

                try {
                    //Save the numbers inputs the Fragment view to they according field
                    numbers.put(FieldNamesInterface.NAMES[2], Long.parseLong(number));

                    numbers.put(FieldNamesInterface.NAMES[3], Long.parseLong(number2));

                    SharedPreferences.Editor prefsEditor = prefs.edit();

                    String jsonData = JsonParser.toJSon(numbers);

                    //Lets save the Json to Sharedpreferences
                    prefsEditor.putString(JSON_FILE, jsonData);

                    prefsEditor.commit();

                    //Send Broadcast to change the view (Fragment)
                    Intent intent = new Intent(FieldNamesInterface.BROADCAST_ACTION_TEXT);

                    intent.setAction(FieldNamesInterface.BROADCAST_ACTION_TEXT);

                    intent.putExtra(FieldNamesInterface.BROADCAST_ACTION_TEXT, FieldNamesInterface.RESULT_VIEW_ACTION);

                    getActivity().sendBroadcast(intent);
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }

            }
        });

        if (numbers == null) {
            return;
        }

        //Lets check if there is saved numbers before. And if there is let place the to views.

        if (numbers.get(FieldNamesInterface.NAMES[2]) != null) {
            oldNumber.setText(numbers.get(FieldNamesInterface.NAMES[2]).toString());
        }

        if (numbers.get(FieldNamesInterface.NAMES[3]) != null) {
            newNumber.setText(numbers.get(FieldNamesInterface.NAMES[3]).toString());
        }

    }
}