package fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.fragment.app.Fragment;
import buutti.fi.buuttitestapplication.R;
import utils.FieldNamesInterface;
import utils.JsonParser;

public class ResultLayoutFragment extends Fragment {

    /**
     * View where result of number calculating is shown.
     * User can choosen from two dropdown list numbers to calculate
     */

    private static final String jsonFile = "numbersFile";
    HashMap<String, Long> numbers = new HashMap<String, Long>();

    String broadCastActionText = "broadCastAction";

    SharedPreferences prefs = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {

        prefs = getActivity().getSharedPreferences(
                getActivity().getApplicationContext().getPackageName(), Context.MODE_PRIVATE);

        // use a default value using new Date()
        String json = ((SharedPreferences) prefs).getString(jsonFile, "");

        numbers = JsonParser.fromJson(json);

        return inflater.inflate(R.layout.result_layout, parent, false);
    }

    // This event is triggered soon after onCreateView().
    // Any view setup should occur here.  E.g., view lookups and attaching view listeners.
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        //View view = inflater.inflate(R.layout.two_inputs_fragments,  false);
        final TextView multiplyTextView = (TextView) view.findViewById(R.id.result_multiply);

        Button saveButton = (Button) view.findViewById(R.id.save_button);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String number = multiplyTextView.getText().toString();

                try {

                    //Save the numbers inputs the Fragment view to they according field
                    numbers.put(FieldNamesInterface.NAMES[4], Long.parseLong(number));

                    SharedPreferences.Editor prefsEditor = prefs.edit();

                    String jsonData = JsonParser.toJSon(numbers);

                    //Lets save the Json data to Sharedpreferences.
                    prefsEditor.putString(jsonFile, jsonData);

                    prefsEditor.commit();

                    //Send Broadcast to change the view (Fragment)
                    Intent intent = new Intent(broadCastActionText);

                    intent.setAction(broadCastActionText);

                    intent.putExtra(broadCastActionText, FieldNamesInterface.FIRST_VIEW_ACTION);

                    getActivity().sendBroadcast(intent);
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }

            }
        });

        if (numbers == null) {
            return;
        }

        //Lets define spinners that used to choose numbers to calculate different Numbers
        final Spinner firstSpinner = (Spinner) view.findViewById(R.id.firstSpinner);

        List<String> categories = new ArrayList<String>();
        categories.add(numbers.get(FieldNamesInterface.NAMES[0]).toString());
        categories.add(numbers.get(FieldNamesInterface.NAMES[1]).toString());

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),  R.layout.spinner_size_layout, categories);

        firstSpinner.setAdapter(dataAdapter);

        final Spinner secondSpinner = (Spinner) view.findViewById(R.id.secondSpinner);

        List<String> categories2 = new ArrayList<String>();
        categories2.add(numbers.get(FieldNamesInterface.NAMES[2]).toString());
        categories2.add(numbers.get(FieldNamesInterface.NAMES[3]).toString());

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.spinner_size_layout, categories2);

        secondSpinner.setAdapter(dataAdapter2);


        firstSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String first = firstSpinner.getSelectedItem().toString();

                String second = secondSpinner.getSelectedItem().toString();

                Long result = Long.parseLong(first) * Long.parseLong(second);

                multiplyTextView.setText(Long.toString(result));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        secondSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String first = firstSpinner.getSelectedItem().toString();

                String second = secondSpinner.getSelectedItem().toString();

                Long result = Long.parseLong(first) * Long.parseLong(second);

                multiplyTextView.setText(Long.toString(result));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }
}